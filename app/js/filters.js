(function() {
    "use strict";


    referralsApp.filter("referralTypes", function() {
        return function(types) {
            switch (types) {
            case 1:
                return "Auto Insurance";
            case 2:
                return "Home Insurance";
            case 3:
                return "Life Insurance";
            case 4:
                return "Health Insurance";
            case 5:
                return "Accidental Death Insurance";
            case 6:
                return "Mortgage";
            case 7:
                return "Commercial Lending";
            }
        }
    });
})();