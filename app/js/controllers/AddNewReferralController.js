(function() {
    "use strict";
    referralsApp.controller("AddNewReferralController", addNewReferralController);

    function addNewReferralController($scope) {
        $scope.saveEvent = function(referral, newEventForm) {
            if (newEventForm.$valid) {
                window.alert("referral " + referral.firstName + " saved!");
            }
        };

        $scope.cancelEdit = function() {
            window.location = "/app/ReferralDetails.html";
        };

    }
})();