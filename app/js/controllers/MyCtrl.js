﻿(function() {
    "use strict";

    referralsApp.controller("MyCtrl", function ($scope) {
        $scope.onDateSelected = function (e) {
            var datePicker = e.sender;
            console.log(datePicker.value());
            $scope.selected = true;
        };
    });
})();