﻿(function() {
    "use strict";

    referralsApp.controller("ReferralTypeController", referralTypeController);
    function referralTypeController($scope) {
        $scope.data = {
            repeatSelect: null,
            availableOptions: [
                {id: "1", name: "Auto Insurance"},
                {id: "2", name: "Home Insurance"},
                {id: "3", name: "Life Insurance"},
                {id: "4", name: "Health Insurance"},
                {id: "5", name: "Accidental Death Insurance"},
                {id: "6", name: "Mortgage"},
                {id: "7", name:  "Commercial Lending"}
            ]
        }
    }
    })();