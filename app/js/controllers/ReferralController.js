(function () {
    "use strict";

    referralsApp.controller("ReferralController", referralController);

    //ReferralController.$inject = ['$scope', 'ReferralData'];

    function referralController($scope, referralData) {

       //scope object has a referral object
        $scope.referrals = referralData.getAvengers();
        $scope.foo = 'bar';
        $scope.array = [1, 2, 3];
    }
})();